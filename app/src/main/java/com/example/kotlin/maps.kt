
package com.example.kotlin

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
//import kotlinx.coroutines.flow.internal.NoOpContinuation.context

class maps : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocation: FusedLocationProviderClient

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        createMarker()
        createMarker2()
        createMarker3()

    }

    private fun createMarker3() {
        val favoritePlace3 = LatLng(4.751197,-74.067183)
        map.addMarker(MarkerOptions().position(favoritePlace3).title("Ropa de segunda oferton"))
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(favoritePlace3, 18f),
            4000,
            null
        )
    }
    private fun createMarker2() {
        val favoritePlace2 = LatLng(4.754138,-74.063329)
        map.addMarker(MarkerOptions().position(favoritePlace2).title("Mi tiendiendita fancy"))
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(favoritePlace2, 18f),
            4000,
            null
        )
    }

    private fun createMarker() {
        val favoritePlace = LatLng(4.7522256213041985,-74.06416383596381)
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
            return
        }

        map.isMyLocationEnabled = true
        map.uiSettings.isMyLocationButtonEnabled = true
        map.uiSettings.isZoomControlsEnabled = true
        map.uiSettings.isCompassEnabled = true


        fusedLocation.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                val ubicacion = LatLng(location.latitude, location.longitude)
                map.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(ubicacion, 1f)
                )
            }
        }
        map.addMarker(MarkerOptions().position(favoritePlace).title("Ropita de segunda"))
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(favoritePlace, 18f),
            4000,
            null

        )
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        createMapFragment()
        fusedLocation = LocationServices.getFusedLocationProviderClient(this)
    }
    private fun createMapFragment() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragmentMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

}