package com.example.kotlin

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.properties.Delegates


class Login : AppCompatActivity() {

    companion object {
        lateinit var useremail: String
        lateinit var providerSession: String
        var email by Delegates.notNull<String>()
        var Name by Delegates.notNull<String>()
        var Birthdate by Delegates.notNull<String>()
        var Phone by Delegates.notNull<String>()
        var passowrdConfirm by Delegates.notNull<String>()
        var password by Delegates.notNull<String>()
        lateinit var etEmail: EditText
        lateinit var etPassword: EditText
        lateinit var etName: EditText
        lateinit var etBirthdate: EditText
        lateinit var etPhone: EditText
        lateinit var etConfirmp: EditText


        lateinit var mAuth: FirebaseAuth
        private val callbackManager = CallbackManager.Factory.create()

    }


    //private lateinit var lyTerms: LinearLayout



    private var RESULT_CODE_GOOGLE_SIGN_IN = 100




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //lyTerms = findViewById(R.id.lyTerms)
        //lyTerms.visibility = View.INVISIBLE

        etEmail = findViewById(R.id.etEmail)
        etPassword = findViewById(R.id.etPassword)
        mAuth = FirebaseAuth.getInstance()

        manageButtonLogin()
        etEmail.doOnTextChanged { text, start, before, count -> manageButtonLogin() }
        etPassword.doOnTextChanged { text, start, before, count -> manageButtonLogin() }

    }

    public override fun onStart() {
        super.onStart()

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) goHome(currentUser.email.toString(), currentUser.providerId)

    }

    // Si doy par a atras me redicirecciona a inicio
    override fun onBackPressed() {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    //Validacion de email y contraseña
    private fun manageButtonLogin() {
        var tvLogin = findViewById<TextView>(R.id.tvLogin)
        email = etEmail.text.toString()
        password = etPassword.text.toString()

        if (TextUtils.isEmpty(password) || ValidateEmail.isEmail(email) == false) {

            tvLogin.setBackgroundResource(R.drawable.rounded_corner_loging)
            tvLogin.isEnabled = false
        } else {
            tvLogin.setBackgroundResource(R.drawable.rounded_corner_login)
            tvLogin.isEnabled = true
        }
    }

    /*Creacion del login */
    fun login(view: View) {
        loginUser()
    }
    //- Autentificacion del usuario, si no esta creado registra pero tiene que aceptar terminos y condciones

    private fun loginUser() {
        email = etEmail.text.toString()
        password = etPassword.text.toString()

        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) goHome(email, "email")
                else {
                    Toast.makeText(this, "There was an error authenticating the given info, try again", Toast.LENGTH_SHORT).show()
                }
            }

    }

    //- Redireccion si el login es correcto al MainActivity o mi home

    private fun goHome(email: String, provider: String) {

        useremail = email
        providerSession = provider

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    // Terminos y condiciones
    fun goTerms(view: View) {
        val intents = Intent(this, TermsActivity::class.java).apply {}
        startActivity(intents)
    }

    fun goRegis(view: View) {
        val intents = Intent(this, Registro::class.java).apply {}
        startActivity(intents)
    }

    // Funcion olvide mi contraseña.
    fun forgotPassword(view: View) {
        //startActivity(Intent(this, ForgotPasswordActivity::class.java))
        resetPassword()
    }

    // se guarda el email y se envia correo con link de restablecimiento de contraseña al correo escrito en el apartado del login.
    private fun resetPassword() {
        var e = etEmail.text.toString()
        if (!TextUtils.isEmpty(e)) {
            mAuth.sendPasswordResetEmail(e)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) Toast.makeText(
                        this,
                        "Email Enviado a $e",
                        Toast.LENGTH_SHORT
                    ).show()
                    else Toast.makeText(
                        this,
                        "No user found with the given email",
                        Toast.LENGTH_SHORT
                    ).show()
                }
        } else Toast.makeText(this, "Indicate an email", Toast.LENGTH_SHORT).show()
    }

    fun callSignInGoogle(view: View) {
        signInGoogle()
    }

    private fun signInGoogle() {
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        var googleSignInClient = GoogleSignIn.getClient(this, gso)
        googleSignInClient.signOut()

        startActivityForResult(googleSignInClient.signInIntent, RESULT_CODE_GOOGLE_SIGN_IN)

    }

    fun callSignInFacebook (view:View){
        signInFacebook()
    }
    private fun signInFacebook(){
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("email"))
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult>{
            override fun onSuccess(result: LoginResult) {
                result.let{
                    val token = it.accessToken
                    val credential = FacebookAuthProvider.getCredential(token.token)
                    mAuth.signInWithCredential(credential).addOnCompleteListener {
                        if (it.isSuccessful){
                            email = it.result.user?.email.toString()
                            goHome(email, "Facebook")
                        }
                        else showError("Facebook")
                    }
                }
            }
            override fun onCancel() {
            }
            override fun onError(error: FacebookException) { showError("Facebook") }
        })
    }


    private fun showError (provider: String){
        Toast.makeText(this, "Error while connecting with $provider", Toast.LENGTH_SHORT)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RESULT_CODE_GOOGLE_SIGN_IN) {

            try {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!

                if (account != null){
                    email = account.email!!
                    val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                    mAuth.signInWithCredential(credential).addOnCompleteListener{
                        if (it.isSuccessful) {
                            var dateRegister = SimpleDateFormat("dd/MM/yyyy").format(Date())
                            var dbRegister = FirebaseFirestore.getInstance()
                            dbRegister.collection("users").document(email).set(hashMapOf(
                                "user" to email,
                                "dateRegister" to dateRegister,
                                "provider" to "Google"
                            ))
                            goHome(email, "Google")
                        }
                        else showError("Google")
                    }
                }

            } catch (e: ApiException) {
                showError("Google")
            }
        }
    }
}
