package com.example.kotlin

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.kotlin.Login.Companion.Birthdate
import com.example.kotlin.Login.Companion.Name
import com.example.kotlin.Login.Companion.Phone
import com.example.kotlin.Login.Companion.etBirthdate
import com.example.kotlin.Login.Companion.etName
import com.example.kotlin.Login.Companion.etPhone
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.Date


class Registro : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        Login.etEmail = findViewById(R.id.etEmail)
        Login.etPassword = findViewById(R.id.etPassword)
        etName =findViewById(R.id.etName)
        etPhone =findViewById(R.id.etPhone)
        etBirthdate =findViewById(R.id.etBirthdate)
        Login.etConfirmp =findViewById(R.id.etConfirmp)
        Login.mAuth = FirebaseAuth.getInstance()

        Login.mAuth = FirebaseAuth.getInstance()

        manageButtonLogin()
        Login.etEmail.doOnTextChanged { text, start, before, count -> manageButtonLogin() }
        Login.etPassword.doOnTextChanged { text, start, before, count -> manageButtonLogin() }

    }

    public override fun onStart() {
        super.onStart()

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) goHome(currentUser.email.toString(), currentUser.providerId)

    }

    //Validacion de email y contraseña
    private fun manageButtonLogin() {
        var tvLogin = findViewById<TextView>(R.id.tvLogin)
        Login.email = Login.etEmail.text.toString()
        Login.password = Login.etPassword.text.toString()

        if (TextUtils.isEmpty(Login.password) || ValidateEmail.isEmail(Login.email) == false) {

            tvLogin.setBackgroundResource(R.drawable.rounded_corner_loging)
            tvLogin.isEnabled = false
        } else {
            tvLogin.setBackgroundResource(R.drawable.rounded_corner_register)
            tvLogin.isEnabled = true
        }
    }

    /*Creacion del login */
    fun login(view: View) {
        loginUser()
    }
    //- Autentificacion del usuario, si no esta creado registra pero tiene que aceptar terminos y condciones

    private fun loginUser() {
        Login.email = Login.etEmail.text.toString()
        Login.password = Login.etPassword.text.toString()

        Login.mAuth.signInWithEmailAndPassword(Login.email, Login.password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) goHome(Login.email, "email")
                else {
                    register()
                }
            }

    }

    //- Redireccion si el login es correcto al MainActivity o mi home
    private fun goHome(email: String, provider: String) {

        Login.useremail = email
        Login.providerSession = provider

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    //- Creacion del registro de usuario al no tener una cuenta
    private fun register() {
        Login.email = Login.etEmail.text.toString()
        Login.password = Login.etPassword.text.toString()
        Name =etName.text.toString()
        Phone =etPhone.text.toString()
        Birthdate =etBirthdate.text.toString()

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(Login.email, Login.password)
            .addOnCompleteListener {
                if (it.isSuccessful) {

                    var dateRegister = SimpleDateFormat("dd/MM/yyyy").format(Date())
                    var dbRegister = FirebaseFirestore.getInstance()
                    dbRegister.collection("users").document(Login.email).set(
                        hashMapOf(
                            "user" to Login.email,
                            "dateRegister" to dateRegister,
                            "name" to Name,
                            "Phone" to Phone,
                            "Birthdate" to Birthdate

                        )
                    )

                    goHome(Login.email, "email")
                } else Toast.makeText(this, "Something went wrong with the provided info, check and try again", Toast.LENGTH_SHORT).show()
            }
    }

    private fun showError (provider: String){
        Toast.makeText(this, "Error while connecting with $provider", Toast.LENGTH_SHORT)
    }

}
